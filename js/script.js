var zoom = 14;
var center = [47.4899731, 19.0760184];
var r = 50;
var interval = 10000;

var mymap = L.map('map').setView(center, zoom);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
 attribution: '',
 maxZoom: 18,
 id: 'mapbox/satellite-v9',
 tileSize: 512,
 zoomOffset: -1,
 accessToken: 'pk.eyJ1IjoiYmJhcm4iLCJhIjoiY2s3NHUxYWQwMHBneDNkbnZyc3pkMGgyOCJ9.l8QCn0mqxCQKLT0YhPUEdA'
}).addTo(mymap);

var content = [
	['/sounds/eszter-csiga.mp3', 47.49333, 19.07258],
	['/sounds/eszter-lángos.mp3', 47.49292, 19.07287],
	['/sounds/eszter-rákóczi.mp3', 47.493, 19.07218],
	['/sounds/eszter-rákóczi-térhaszn.mp3', 47.49294, 19.07162],
	['/sounds/ica-lakhatás.mp3', 47.4928, 19.07444],
	['/sounds/marietta-káosz.mp3', 47.49251, 19.07974],
	['/sounds/marietta-mindig.mp3', 47.48719, 19.08789],
	['/sounds/marietta-mitől.mp3', 47.48966, 19.07645],
	['/sounds/marietta-férfiak.mp3', 47.48935, 19.07844],
	['/sounds/marietta-rendőrök.mp3', 47.48692, 19.08531],
	['/sounds/mariann-változás.mp3', 47.49315, 19.0696],
	['/sounds/mariann-emberek.mp3', 47.49174, 19.06896],
	['/sounds/eszter-bicó.mp3', 47.4883, 19.10016],
	['/sounds/mariann-tömő.mp3', 47.48512, 19.08047],
	['/sounds/ica-fotózás.mp3', 47.49663, 19.07008],
	['/sounds/fanni-6.mp3', 47.49152, 19.07919],
	['/sounds/fanni-barát.mp3', 47.48941, 19.07109],
	['/sounds/fanni-költözés.mp3', 47.4896, 19.07249],
	['/sounds/fanni-szerelem.mp3', 47.4894, 19.06956],
	['/sounds/domi-blahameki.mp3', 47.49602, 19.07108],
	['/sounds/domi-gólyából.mp3', 47.49183, 19.08846],
	['/sounds/domi-hintaló.mp3', 47.49481, 19.07191],
	['/sounds/domi-support.mp3', 47.4865, 19.07999],
	['/sounds/domi-temető.mp3', 47.49719, 19.08261],
	['/sounds/domi-boltok.mp3', 47.49624, 19.06904],
	['/sounds/gréta-baross1.mp3', 47.48968, 19.07439],
	['/sounds/gréta-baross2.mp3', 47.48881, 19.08339],
	['/sounds/gréta-corvin.mp3', 47.48586, 19.07679],
	['/sounds/gréta-első.mp3', 47.48648, 19.08916],
	['/sounds/gréta-kálvária.mp3', 47.48874, 19.08573],
	['/sounds/gréta-mátyás.mp3', 47.49199, 19.07957],
	['/sounds/gréta-sztorik.mp3', 47.48794, 19.08794]
]

var icon_active = L.icon({
	iconUrl : 'icon-active.svg',
	iconSize : [30, 30]
});

var icon_inactive = L.icon({
	iconUrl : 'icon-inactive.svg',
	iconSize : [30, 30]
});

var icon_user = L.icon({
	iconUrl : 'icon-user.svg',
	iconSize : [30, 30]
});

for (let i = content.length - 1; i >= 0; i--) {
	content[i][4] = new Audio(content[i][0]);
	content[i][3] = L.marker([content[i][1], content[i][2]], {
		opacity : 1.0,
		icon : icon_inactive
	}).addTo(mymap);
	function fuckIOS(i){
		content[i][3].on('click', function(){
				playAudio(i);
				content[i][3].off();
				setTimeout(function(){
					fuckIOS(i);
				}, 300);
		});
	}
	fuckIOS(i);
}

function playAudio(i) {
	let audio = content[i][4];
	if (audio.paused) {
		for (let i = content.length - 1; i >= 0; i--) {
			content[i][4].pause();
			content[i][4].currentTime = 0;
			content[i][3].setIcon(icon_inactive);
		}
		content[i][3].setIcon(icon_active);
		audio.play();
	} else {
		for (let i = content.length - 1; i >= 0; i--) {
			content[i][4].pause();
			content[i][3].setIcon(icon_inactive);
			content[i][4].currentTime = 0;
		}
	}
}

$('.mi-ez').click(function(){
	$('.mobile-nav').fadeOut();
	$('.map').fadeOut();
	$('.mi-ez-content').fadeIn();
	$('.mobile-header .title').text('mi ez?').css({
		"font-family" : "'Geo', monospace",
		"font-size" : "125%"
	});
	$('.kuldj-hangot-content').fadeOut();
	$('.nav-item').removeClass('active');
	$('.link').removeClass('active');
	$('.mi-ez').addClass('active');
});

$('.kuldj-hangot').click(function(){
	$('.mobile-nav').fadeOut();
	$('.map').fadeOut();
	$('.kuldj-hangot-content').fadeIn();
	$('.mobile-header .title').text('küldj hangot!').css({
		"font-family" : "'JetBrains Mono', monospace"
	});
	$('.mi-ez-content').fadeOut();
	$('.nav-item').removeClass('active');
	$('.kuldj-hangot').addClass('active');
});

$('.hangterkep').click(function(){
	$('.mobile-nav').fadeOut();
	$('.map').fadeIn();
	$('.mobile-header .title').text('hangtérkép').css({
		"font-family" : "'VT323', monospace",
		"font-size" : "140%"
	});
	$('.mi-ez-content').fadeOut();
	$('.kuldj-hangot-content').fadeOut();
	$('.nav-item').removeClass('active');
	$('.link').removeClass('active');
	$('.hangterkep').addClass('active');
});

$('.open-button').click(function(){
	$('.mobile-nav').fadeIn();
	$('.close-button').fadeIn();
});

$('.close-button').click(function(){
	$('.mobile-nav').fadeOut();
});

function getLocation(){
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
		function getCurrentPos() {
			navigator.geolocation.getCurrentPosition(showPosition);
		}
		setInterval(getCurrentPos, interval);
	} else {
		alert('A böngésződ nem támogatja a helyadatokat.');
	}
}

function	showPosition(position){
	latlng = L.latLng(position.coords.latitude, position.coords.longitude);
	user.setLatLng(latlng);
	for (var i = content.length - 1; i >= 0; i--) {
		let p = content[i][3]._latlng;
		let d = p.distanceTo(latlng);
		if (d < r) {
			if (isMobile.apple.device) {
				function fuckIOS(i){
					content[i][3].on('click', function(){
							playAudio(i);
							content[i][3].off();
							setTimeout(function(){
								fuckIOS(i);
							}, 300);
					});
				}
				fuckIOS(i);
			} else {
				content[i][3].on('click', function(){
					playAudio(i);
				});
			}
		} else {
			content[i][3].setIcon(icon_inactive).setOpacity(0.5);
			content[i][3].off();
			if (isMobile.apple.device) {
				function fuckIOS(i){
					content[i][3].on('click', function(){
							alert('Menj közelebb ehhez a ponthoz, hogy meghallgathasd!');
							content[i][3].off();
							setTimeout(function(){
								fuckIOS(i);
							}, 300);
					});
				}
				fuckIOS(i);
			} else {
				content[i][3].on('click', function(){
					alert('Menj közelebb ehhez a ponthoz, hogy meghallgathasd!');
				});
			}
		}
	}
}

function showError(error){
	switch(error.code) {
    case error.PERMISSION_DENIED:
      alert("Letiltottad a helyadatokat.");
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Nem elérhetőek a helyadatok.");
      break;
    case error.TIMEOUT:
      alert("Időtúllépés.");
      break;
    case error.UNKNOWN_ERROR:
      alert("Ismeretlen hiba.");
      break;
  }
}

if (isMobile.any) {
	var user = L.marker([0, 0], {
		icon : icon_user
	}).addTo(mymap);

	var centeruser = L.easyButton('&plus;', function(btn, map) {
	   map.flyTo(user._latlng, zoom);
	}).addTo(mymap);

	var centersounds = L.easyButton('&#9675;', function(btn, map) {
	   map.flyTo(center, zoom);
	}).addTo(mymap);

	getLocation();
}